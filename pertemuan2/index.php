<?php 
    //comment
    /* also comment
    
    to add comment to existing lines, block lines then ctrl + /

    */

    // standar output
    echo "Taufiq"; //string bisa pake '' atau ""
    print "Taufiq";
    print_r ("Taufiq");
    var_dump ("Taufiq");

    echo true;
    echo false;

    $nama = "taufiq";

    echo "<br>";

    echo nl2br ("\n $nama\n");

    echo nl2br ('\n $nama\n');
    
    // PHP di dalam HTML / HTML di dalam PHP

    // Operator 
        // aritmatika                   + - * / %
        // concatenation/concat string      .
        // assignment                   = += -= *= /= %= .=
        // perbandingan                 < > <= >= == !=
        // perbandingan dengan tipe data === !==
        // logika                       && || !


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Belajar PHP</title>
</head>
<body>
    <!-- php di dalam html (banyak digunakan)-->

    <h1>Halo, Selamat Datang <?php echo "Taufiq"; ?></h1>
    <p><?php echo "ini adalah paragraf"; ?></p>

    <!-- html di dalam php -->
    <?php echo "<h1>Halo, Selamat Datang Taufiq</h1>";?>

</body>
</html>